import slackclient,time
import pickle  # would be used for saving temp files
import random  # used to get a random number
import numpy as np
import pandas as pd 
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from nltk.stem import WordNetLemmatizer 
from nltk.corpus import wordnet
import re
lemmatizer = WordNetLemmatizer() 
# delay in seconds before checking for new events 
SOCKET_DELAY = 1

# slackbot environment variables
ELEARN_SLACK_NAME = 'elearning_bot'
ELEARN_SLACK_TOKEN = 'xoxb-16821617108-710056643060-3RiSjMiMKkDPKqbU2iT4B2Eq'
ELEARN_SLACK_ID ='ULW1NJX1S'
# initialize slack client
elearn_slack_client = slackclient.SlackClient(ELEARN_SLACK_TOKEN)
# check if everything is alright
print(ELEARN_SLACK_NAME)
print(ELEARN_SLACK_TOKEN)
is_ok = elearn_slack_client.api_call("users.list").get('ok')
print(is_ok)

# find the id of our slack bot
if(is_ok):
    for user in elearn_slack_client.api_call("users.list").get('members'):
        if user.get('name') == ELEARN_SLACK_NAME:
            print(user.get('id'))

def is_private(event):
    """Checks if private slack channel"""       
    return event.get('channel').startswith('D')
# how the bot is mentioned on slack
def get_mention(user):
    return '<@{user}>'.format(user=user)

elearn_slack_mention = get_mention(ELEARN_SLACK_ID)    
def is_for_me(event):
    """Know if the message is dedicated to me"""
    # check if not my own event
    type = event.get('type')
    #if type and event.get('user') and type == 'message' and event.get('user') not in [ELEARN_SLACK_ID,'USLACKBOT'] :
    if type and event.get('user') and type == 'message' and event.get('user') not in [ELEARN_SLACK_ID] and not "subtype" in event:
        # in case it is a private message return true
        if is_private(event):
            return True
        # in case it is not a private message check mention
        text = event.get('text')
        channel = event.get('channel')
        if elearn_slack_mention in text.strip().split():
            return True
def is_hi(message):
        tokens = [word.lower() for word in message.strip().split()]
        return any(g in tokens
                   for g in ['hello', 'bonjour', 'hey', 'hi', 'sup', 'morning', 'hola', 'ohai', 'yo'])


def is_bye(message):   
        tokens = [word.lower() for word in message.strip().split()]
        return any(g in tokens
                   for g in ['bye', 'goodbye', 'revoir', 'adios', 'later', 'cya'])
def say_hi(user_mention):
    """Say Hi to a user by formatting their mention"""
    response_template = random.choice(['Sup, {mention}...',
                                       'Yo!',
                                       'Hola {mention}',
                                       'Bonjour!'])
    return response_template.format(mention=user_mention)


def say_bye(user_mention):
    """Say Goodbye to a user"""
    response_template = random.choice(['see you later, alligator...',
                                      'adios amigo',
                                       'Bye {mention}!',
                                       'Au revoir!'])
    return response_template.format(mention=user_mention)
def get_wordnet_pos(word):
    """Map POS tag to first character lemmatize() accepts"""
    tag = nltk.pos_tag([word])[0][1][0].upper()
    tag_dict = {"J": wordnet.ADJ,
                "N": wordnet.NOUN,
                "V": wordnet.VERB,
                "R": wordnet.ADV}
    return tag_dict.get(tag, wordnet.NOUN)
def tokenize_and_lemm(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    lemma = [lemmatizer.lemmatize(t, get_wordnet_pos(t)) for t in filtered_tokens]
    return lemma

def talk_to_elearning_ChatBot(test_set_sentence):
    tfidf_vectorizer_pikle_path = "ChatBotData/tfidf_vectorizer.pickle"
    tfidf_matrix_train_pikle_path ="ChatBotData/tfidf_matrix_train.pickle"
    
    ##--------------to use------------------#
    f = open(tfidf_vectorizer_pikle_path, 'rb')
    tfidf_vectorizer = pickle.load(f)
    f.close()

    f = open(tfidf_matrix_train_pikle_path, 'rb')
    tfidf_matrix_train = pickle.load(f)
    f.close()
    # ----------------------------------------#
    # enter your test sentence
    test_set = (test_set_sentence, "")
        # use the learnt dimension space
    # to run TF-IDF on the query
    tfidf_matrix_test = tfidf_vectorizer.transform(test_set)

    # then run cosine similarity between the 2 tf-idfs
    cosine = cosine_similarity(tfidf_matrix_test, tfidf_matrix_train)
    cosine = cosine[0]
    max = cosine.max()
    # if score is more than 0.7
    if (max > 0.7): 
        # we can afford to get multiple high score documents to choose from
        new_max = max - 0.01
        
        # load them to a list
        list = np.where(cosine > new_max)
        
        # choose a random one to return to the user 
        # this happens to make Lina answers diffrently to same sentence
        response_index = random.choice(list[0])

    else:
        # else we would simply return the highest score
        response_index = np.where(cosine == max)[0]
    VideoData = pd.read_csv("ChatBotData/VideoData.csv",sep=';', encoding='ISO-8859-1')
    text = "I think its about " +'*'+ VideoData['Category'][response_index]+'*' + "\nThis video might help you\n" + VideoData['Hyperlink'][response_index]
    return text
def handle_message(message, user, channel):
    if is_hi(message):
        user_mention = get_mention(user)
        post_message(message=say_hi(user_mention), channel=channel)
    elif is_bye(message):
        user_mention = get_mention(user)
        post_message(message=say_bye(user_mention), channel=channel)
    else:
        user_mention = get_mention(user)
        post_message(message=talk_to_elearning_ChatBot(message), channel=channel)
def post_message(message, channel):
    elearn_slack_client.api_call('chat.postMessage', channel=channel,
                          text=message, as_user=True)
def run():
    if elearn_slack_client.rtm_connect():
        print('[.] E Learning Machine is ON...')
        while True:
            event_list = elearn_slack_client.rtm_read()
            if len(event_list) > 0:
                for event in event_list:
                    print(event)
                    if is_for_me(event):
                        handle_message(message=event.get('text'), user=event.get('user'), channel=event.get('channel'))
            time.sleep(SOCKET_DELAY)
    else:
        print('[!] Connection to Slack failed.')

if __name__=='__main__':
    run()