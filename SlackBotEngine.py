# -*- coding: utf-8 -*-
"""
Created on Wed Oct  2 10:49:48 2019

@author: lahloumo


Description : Ce Script est un Module qui permet d'indexer les vidéos 
d'une chaine d'elearning en suivant les étapes suivantes :
    - Extraction des métadonnées des vidéos
    - Speech Recognition
    - Keyword Extraction
    - Document Clustering
    - Création d'une arboréscance de Vidéos
"""

""" Importation des librairies """

import numpy as np
import pandas as pd
import youtube_dl
import webvtt
from youtube_transcript_api import YouTubeTranscriptApi
import speech_recognition as sr
import pke
from nltk.corpus import stopwords
import string
from nltk.tokenize import word_tokenize
import nltk
from nltk.stem import WordNetLemmatizer 
lemmatizer = WordNetLemmatizer() 
import re
from nltk.corpus import wordnet
from sklearn.feature_extraction.text import TfidfVectorizer
from scipy.cluster.hierarchy import dendrogram,linkage
import matplotlib.pyplot as plt
import pickle  # would be used for saving temp files


"""Importation des Video e-learning """
VideoData = pd.read_csv("ChatBotData/VideoData.csv",sep=';', encoding='ISO-8859-1')
###paramétrage de Youtube_DL###
ydl_opts = {
        'format': 'bestaudio/best',
        'writesubtitles':'yes',
        'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'wav',
                'preferredquality': '320',}],
}
"""Extraction des métadonnées des vidéos"""
for i,a in enumerate(VideoData['Hyperlink']):
    if  str(VideoData['Transcript'][i]) == 'nan':
        ###Extraction de l'audio ###
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            metadata = ydl.extract_info(a,download=True)
        ###Extraction du Sous-Titres###
        if metadata['subtitles']:
            transcript = ""
            for caption in webvtt.read(metadata['title']+'-'+metadata['id']+'.'
                                       +list(metadata['requested_subtitles'].keys())[0]+'.vtt'):
                transcript = transcript + caption.text + " "
                transcript = transcript.replace("\n", " ")
                
            automaticsub= YouTubeTranscriptApi.get_transcript(metadata['id'])    
            transcript_auto =""
            for sub in automaticsub :
                transcript_auto = transcript_auto + sub['text'] + " "
            
        else:
            automaticsub= YouTubeTranscriptApi.get_transcript(metadata['id'])
            transcript = ""
            for sub in automaticsub :
                transcript = transcript + sub['text'] + " "
        VideoData['Transcript'][i]=transcript
    ##os.remove("ChangedFile.csv")
VideoData.to_csv("ChatBotData/VideoData.csv",sep=';', encoding='utf-8',index=False)

"""Création de l'arborescance des vidéos en se basant sur les mots clés"""
############ here we define a tokenizer and stemmer which returns###########
########### the set of lemma in the text that it is passed #################
def get_wordnet_pos(word):
    """Map POS tag to first character lemmatize() accepts"""
    tag = nltk.pos_tag([word])[0][1][0].upper()
    tag_dict = {"J": wordnet.ADJ,
                "N": wordnet.NOUN,
                "V": wordnet.VERB,
                "R": wordnet.ADV}
    return tag_dict.get(tag, wordnet.NOUN)

def tokenize_and_lemm(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    lemma = [lemmatizer.lemmatize(t, get_wordnet_pos(t)) for t in filtered_tokens]
    return lemma

def tokenize_only(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    return filtered_tokens

#define vectorizer parameters
tfidf_vectorizer = TfidfVectorizer(max_df=0.8,
                                 min_df=2, stop_words='english',
                                 use_idf=True, tokenizer=tokenize_and_lemm, ngram_range=(1,5))

tfidf_matrix = tfidf_vectorizer.fit_transform(VideoData['Transcript']) #fit the vectorizer to Transcript 0.039

terms = tfidf_vectorizer.get_feature_names()
tfidf = tfidf_matrix.todense()

#define similarity matrix
from sklearn.metrics.pairwise import cosine_similarity
dist = 1 - cosine_similarity(tfidf_matrix)

tfidf_vectorizer_pikle_path = "ChatBotData/tfidf_vectorizer.pickle"
tfidf_matrix_train_pikle_path ="ChatBotData/tfidf_matrix_train.pickle"

f = open(tfidf_vectorizer_pikle_path, 'wb')
pickle.dump(tfidf_vectorizer, f)
f.close()

f = open(tfidf_matrix_train_pikle_path, 'wb')
pickle.dump(tfidf_matrix, f)
f.close()